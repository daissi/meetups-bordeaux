---
title: "Meetup Debian à Bordeaux 🍥"
colorlinks: true
---
<link href="Deb-BDX-logo-B-det-small.png" rel="icon" type="image/x-icon">
<a href="https://salsa.debian.org/daissi/meetups-bordeaux/"><img style="position: absolute; top: 0; right: 0; border: 0;" src="https://lamby.pages.debian.net/salsa-ribbons/img/gray_right.png" alt="Fork me on Salsa"></a>
<a href="https://salsa.debian.org/daissi/meetups-bordeaux/"><img src="Deb-BDX-logo-B-det-small.png" alt="Debian Bordeaux Meetup" align="right"></a>

Ces meetups permettent de rassembler la communauté Debian bordelaise (utilisateurs, contributeurs ou juste curieux) régulièrement.\
Le but de ses rassemblements est de partager, discuter/présenter un sujet 💬, contribuer à Debian 🍥, chasser les bugs 🐛 ou créer de nouveaux paquets 📦.\
Bref, c'est surtout l'occasion de faire vivre Debian *In Real Life* et cela est rendu possible grâce aux amis listés dans *Liens*.

## 📓 Sujets
- Packaging, l'art de créer des paquets selon la [politique Debian](https://www.debian.org/doc/debian-policy/)
- Gestion des [clés gpg](https://www.debian.org/events/keysigning.fr.html) et signature des paquets Debian
- Les principes du logiciel libre selon Debian ([DFSG](https://www.debian.org/social_contract.fr.html#guidelines))
- DFSG et les problèmes soulevés par les modèles de machine/deep learning ([ML-Policy](https://salsa.debian.org/deeplearning-team/ml-policy/-/blob/master/ML-Policy.rst))
- [PipeWire](https://pipewire.org/)/[WirePlumber](https://pipewire.pages.freedesktop.org/wireplumber/), le nouveau serveur audio sous Bookworm
- [Mobian](https://mobian-project.org/), la distro dérivé de Debian ciblant les smartphones
- [Apertis](https://www.apertis.org/), la dérivé conçue à l'origine pour l'automobile mais qui sert également de base pour l'OS de l'[Atari VCS](https://atari.com/pages/atari-vcs)
- [Backdoor xz](https://tukaani.org/xz-backdoor/) et conséquences pour Debian
- [Bug Squashing Party](https://wiki.debian.org/fr/BSP), préparons le terrain pour [Trixie](https://wiki.debian.org/DebianTrixie) (~ été 2025)
- etc, proposez vos sujets via le [sondage](https://framaforms.org/sujets-de-meetups-debian-1712591845). Les speakers sont également les bienvenu(e)s.

## 🌍 Lieu

- <a href="https://yack-coworking.fr/"><img src="Yack.png" alt="Logo Yack" width="50"></a> [Yack coworking](https://yack-coworking.fr/) à Bègles, voir la [carte OSM](https://www.openstreetmap.org/?mlat=44.81309&mlon=-0.55185#map=19/44.81309/-0.55185)

## 🕰 Date & heure

- Jeudi 16 Mai 2024 à 18:30 - 20:30, sur le thème du packaging 📦 - Voir l'[Annonce](https://france.debian.net/posts/2024/Meetup_01_Bordeaux/) et le [Retour](https://france.debian.net/posts/2024/Retour_Meetup_01_Bordeaux/)
- Jeudi 13 Juin 2024 à 18:00 - 20:00, Bug Squashing Party 🐛 et contribution Debian - Voir l'[Annonce](https://france.debian.net/posts/2024/Meetup_02_Bordeaux/)
- **Jeudi 26 Septembre 2024 à 18:30 TBC**, [Les principes du logiciel libre selon Debian](https://www.debian.org/social_contract.fr.html#guidelines) ⚖ - Plus d'info sur [Mobilizon](https://meet.debian.net/events/d5303cef-1f18-41a0-8a96-90d0fcf574f0)
- Puis tous les trimestres.

## 📬 Contact
- [daissi\@debian.org](mailto:daissi@debian.org?subject=Meetup Debian Bordeaux)
- [Liste de diffusion de Debian France](https://france.debian.net/mailman3/hyperkitty/list/asso@france.debian.net/latest): **asso@france.debian.net**
- IRC: **#debian-france** (OFTC).

## 🕸 Liens
<a href="https://www.debian.org/"><img src="openlogo.svg" alt="Logo Debian" width="100"></a>
<a href="https://france.debian.net/"><img src="debian-france.svg" alt="Logo Debian France" width="150"></a>
<a href="https://yaal.coop/"><img src="yaal-circle-large-2.webp" alt="Logo Yaal" width="150"></a>
<a href="https://yack-coworking.fr/"><img src="Yack.png" alt="Logo Yack" width="275"></a>
<a href="https://www.collabora.com/"><img src="Collabora.svg" alt="Logo Collabora" width="225"></a>
