#!/usr/bin/make -f

all:
	pandoc -s README.md -o index.html
	mkdir -p public
	mv index.html public
	cp -R Images/* public
